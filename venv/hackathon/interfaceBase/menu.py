import sys
import logging
from PyQt5.QtWidgets import *
from composants.regle import regle
from composants.serie import serie

class menuPrincipale(QMainWindow):
    def __init__(self, gestionnaireBD):
        super().__init__()
        self.bd = gestionnaireBD
        
        self.composant = None
        self.info = None
        self.superLayout = None

    def affiche(self):
        
        menu = self.menuBar()

        #menu Fichier
        exitAction = QAction('&Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setStatusTip("Quitter l'application")
        exitAction.triggered.connect(qApp.exit)

        initbdAction = QAction('&Init BD', self)
        initbdAction.setStatusTip("Initialiser la bd")
        initbdAction.triggered.connect(self.initBD)
        
        fichierMenu = menu.addMenu("&Fichier")
        fichierMenu.addAction(exitAction)
        fichierMenu.addAction(initbdAction)

        #menu pour les règles
        listerRegleAction = QAction('&Lister les règles', self)
        listerRegleAction.triggered.connect(self.listerRegle)
        ajouterRegleAction = QAction('&Ajouter une règle', self)
        ajouterRegleAction.triggered.connect(self.ajouterRegle)
        modifierRegleAction = QAction('&Modifier une règle', self)
        modifierRegleAction.triggered.connect(self.modifierRegle)
        effacerRegleAction = QAction('&Effacer une règle', self)
        effacerRegleAction.triggered.connect(self.effacerRegle)
        regleMenu = menu.addMenu("&Règles")
        regleMenu.addAction(listerRegleAction)
        regleMenu.addAction(modifierRegleAction)
        regleMenu.addAction(ajouterRegleAction)
        regleMenu.addAction(effacerRegleAction)

        # menu pour les séries d'exercices
        listerSerieAction = QAction('&Lister les séries', self)
        listerSerieAction.triggered.connect(self.listerSerie)
        ajouterSerieAction = QAction('&Ajouter une série', self)
        ajouterSerieAction.triggered.connect(self.ajouterSerie)
        modifierSerieAction = QAction('&Modifier une série', self)
        modifierSerieAction.triggered.connect(self.modifierSerie)
        effacerSerieAction = QAction('&Effacer une série', self)
        effacerSerieAction.triggered.connect(self.effacerSerie)
        regleMenu = menu.addMenu("&Séries")
        regleMenu.addAction(listerSerieAction)
        regleMenu.addAction(modifierSerieAction)
        regleMenu.addAction(ajouterSerieAction)
        regleMenu.addAction(effacerSerieAction)

        self.setGeometry(300, 300, 500, 500)
        self.setWindowTitle('Fenêtre principale')
        self.statusBar().showMessage('Barre de statut')
    
        self.show()
        self.viderEcran()
  
    # Initialisation de la base de données
    #
    def initBD(self):
        """Initialise la BD"""
        bdOuverte = self.bd.initialise()
        if not bdOuverte['ok']:
            buttonReply = QMessageBox.critical(self, 'Erreur dans la BD',
                                               'Une erreur c\'est produite \n'
                                               + bdOuverte['msg'] + '\n'
                                               'Appuyez sur OK pour continuer' ,
                                               QMessageBox.Ok, QMessageBox.Ok
                                               )
            
            
    #############################################
    ## CRUD des règles
    
    # Ajouter une règle dans la bd
    def ajouterRegle(self):
        self.composant = regle(self, self.bd)
        self.ecranAjouter()
 
    # Modifier une règle dans la bd
    def modifierRegle(self):
        self.composant = regle(self, self.bd)
        self.ecranModifier()
        
    # Effacer une règle dans la bd
    def effacerRegle(self):
        self.statusBar().showMessage('')
        self.composant = regle(self, self.bd)
        self.ecranEffacer()

    # Lister toutes les règles
    def listerRegle(self):
        self.composant = regle(self, self.bd)
        self.ecranLister()

    #############################################
    ## CRUD des séries

    # Ajouter une série dans la bd
    def ajouterSerie(self):
        self.composant = serie(self, self.bd)
        self.ecranAjouter()

    # Modifier une série dans la bd
    def modifierSerie(self):
        self.composant = serie(self, self.bd)
        self.ecranModifier()

    # Effacer une série dans la bd
    def effacerSerie(self):
        self.statusBar().showMessage('')
        self.composant = serie(self, self.bd)
        self.ecranEffacer()

    # Lister toutes les séries
    def listerSerie(self):
        self.composant = serie(self, self.bd)
        self.ecranLister()

    #############################################
    ## fonctions génériques pour les CRUD

    # Écran générique pour ajouter un composant
    def ecranAjouter(self):
        contenu = self.composant.interfaceAjout()

        boutonLayout = QHBoxLayout()
        sauvegardeButton = QPushButton("Creer")
        sauvegardeButton.clicked.connect(self.creer)
        annulerButton = QPushButton("Annuler")
        annulerButton.clicked.connect(self.resetEcran)
        boutonLayout.addStretch(1)
        boutonLayout.addWidget(sauvegardeButton)
        boutonLayout.addStretch(1)
        boutonLayout.addWidget(annulerButton)
        boutonLayout.addStretch(1)
        
        infoLayout = QHBoxLayout()
        self.info = QLabel('')
        infoLayout.addWidget(self.info)
        self.superLayout = QVBoxLayout()
        self.superLayout.addLayout(contenu)
        self.superLayout.addLayout(infoLayout)
        self.superLayout.addLayout(boutonLayout)
        self.superLayout.addStretch(1)
        
        fenetre = QWidget()
        fenetre.setLayout(self.superLayout)
        self.setCentralWidget(fenetre)

    # Écran générique pour lister un composant
    def ecranLister(self):
        contenu = self.composant.interfaceLister(self.afficherItem)

        boutonLayout = QHBoxLayout()
        
        annulerButton = QPushButton("Retour")
        annulerButton.clicked.connect(self.resetEcran)
        boutonLayout.addStretch(1)
        boutonLayout.addWidget(annulerButton)
        boutonLayout.addStretch(1)

        infoLayout = QHBoxLayout()
        self.info = self.composant.interfaceAfficheItemInit()
        infoLayout.addLayout(self.info)
        
        self.superLayout = QVBoxLayout()
        self.superLayout.addLayout(contenu)
        self.superLayout.addLayout(infoLayout)
        self.superLayout.addLayout(boutonLayout)
        self.superLayout.addStretch(1)

        fenetre = QWidget()
        fenetre.setLayout(self.superLayout)
        self.setCentralWidget(fenetre)

    # Écran générique pour effacer un composant
    def ecranEffacer(self):
        contenu = self.composant.interfaceLister(self.afficherItem)

        boutonLayout = QHBoxLayout()
        effacementButton = QPushButton("Effacer")
        effacementButton.clicked.connect(self.effacer)
        annulerButton = QPushButton("Retour")
        annulerButton.clicked.connect(self.resetEcran)
        boutonLayout.addStretch(1)
        boutonLayout.addWidget(effacementButton)
        boutonLayout.addStretch(1)
        boutonLayout.addWidget(annulerButton)
        boutonLayout.addStretch(1)

        infoLayout = QHBoxLayout()
        self.info = self.composant.interfaceAfficheItemInit()
        infoLayout.addLayout(self.info)

        self.superLayout = QVBoxLayout()
        self.superLayout.addLayout(contenu)
        self.superLayout.addLayout(infoLayout)
        self.superLayout.addLayout(boutonLayout)
        self.superLayout.addStretch(1)

        fenetre = QWidget()
        fenetre.setLayout(self.superLayout)
        self.setCentralWidget(fenetre)

    # Écran générique pour modifier un composant
    def ecranModifier(self):
        contenu = self.composant.interfaceLister(self.afficherItem)

        boutonLayout = QHBoxLayout()
        effacementButton = QPushButton("Modifier")
        effacementButton.clicked.connect(self.modifier)
        annulerButton = QPushButton("Retour")
        annulerButton.clicked.connect(self.resetEcran)
        boutonLayout.addStretch(1)
        boutonLayout.addWidget(effacementButton)
        boutonLayout.addStretch(1)
        boutonLayout.addWidget(annulerButton)
        boutonLayout.addStretch(1)

        infoLayout = QHBoxLayout()
        self.info = self.composant.interfaceAfficheItemInit()
        infoLayout.addLayout(self.info)

        self.superLayout = QVBoxLayout()
        self.superLayout.addLayout(contenu)
        self.superLayout.addLayout(infoLayout)
        self.superLayout.addLayout(boutonLayout)
        self.superLayout.addStretch(1)

        fenetre = QWidget()
        fenetre.setLayout(self.superLayout)
        self.setCentralWidget(fenetre)

    # Écran générique pour modifier un composant
    def ecranModifierItem(self, id):
        contenu = self.composant.interfaceModifier(id)

        boutonLayout = QHBoxLayout()
        sauvegardeButton = QPushButton("Sauvegarder")
        sauvegardeButton.clicked.connect(self.sauvegarder)
        annulerButton = QPushButton("Annuler")
        annulerButton.clicked.connect(self.resetEcran)
        boutonLayout.addStretch(1)
        boutonLayout.addWidget(sauvegardeButton)
        boutonLayout.addStretch(1)
        boutonLayout.addWidget(annulerButton)
        boutonLayout.addStretch(1)

        infoLayout = QHBoxLayout()
        self.info = QLabel('')
        infoLayout.addWidget(self.info)

        self.superLayout = QVBoxLayout()
        self.superLayout.addLayout(contenu)
        self.superLayout.addLayout(infoLayout)
        self.superLayout.addLayout(boutonLayout)
        self.superLayout.addStretch(1)

        fenetre = QWidget()
        fenetre.setLayout(self.superLayout)
        self.setCentralWidget(fenetre)

    # Afficher un composant
    def afficherItem(self):
        self.composant.interfaceAfficheItem(self.composant.idItemChoisi())
        
    
    # creation du composant
    def creer(self):
        #valide les infos
        validation = self.composant.estValide()
        if validation['ok']:
            self.composant.creer()
            self.resetEcran()
        else:
            msgErreur = ''
            for erreur in validation['msg']:
                msgErreur = msgErreur+erreur + ','
            self.statusBar().showMessage(msgErreur)

            
    # Modifier un composant
    def modifier(self):
        self.viderEcran()
        self.ecranModifierItem(self.composant.idItemChoisi())

    # sauvegarde du composant modifié
    def sauvegarder(self):
        # valide les infos
        validation = self.composant.estValide()
        if validation['ok']:
            self.composant.sauvegarder()
            self.resetEcran()
            self.statusBar().showMessage("L'item a été modifié")
        else:
            msgErreur = ''
            for erreur in validation['msg']:
                msgErreur = msgErreur + erreur + ','
            self.statusBar().showMessage(msgErreur)
            
           
    # Effacer un composant
    def effacer(self):
        self.composant.effacer(self.composant.idItemChoisi()) #il faudrait demander de confirmer
        self.statusBar().showMessage("L'item a été effacé")
        self.ecranEffacer()
    
    # Efface l'écran
    def viderEcran(self):
        vide = QLabel('')
        self.setCentralWidget(vide)
        self.statusBar().showMessage('')
        
    # Reset  l'écran
    def resetEcran(self):
        self.viderEcran()
        self.composant = None