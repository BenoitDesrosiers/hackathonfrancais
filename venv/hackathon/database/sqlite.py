import sys
from PyQt5 import QtSql
from PyQt5.QtWidgets import QMessageBox, qApp

class bd():
    def __init__(self, typeBD,nomBD):
        self.nomBD = nomBD
        self.typeBD = typeBD
        self.bd = None
        
    def ouverture(self):
        self.bd = QtSql.QSqlDatabase.addDatabase(self.typeBD)
        self.bd.setDatabaseName(self.nomBD)
        ok = True
        msg = ''
        if not self.bd.open():
            ok = False
            msg ='impossible d\'ouvrir la base de données'
        return {'ok': ok, 'msg': msg}
        
    # Création des tables et insertion de données de test. 
    def initialise(self):
        requete = QtSql.QSqlQuery()
        
        #Création des tables
        msg = ''
        ok = requete.exec("CREATE TABLE regles( "
                     "id integer primary key, "
                     "titre varchar(200), "
                     "description varchar(2000)"
                     ")") 
        if not ok:
            msg =  'table regles non créée'
         
        if ok:
            ok = requete.exec("CREATE TABLE seriesExercices( "
                     "id integer primary key, "
                     "regleId int,"
                     "titre varchar(200), "
                     "description varchar(2000),"
                     "FOREIGN KEY(regleId) REFERENCES regles(id) "
                     "ON DELETE CASCADE "
                     "ON UPDATE CASCADE"
                     ")")
            if not ok:
                msg ='table series non créée'
        
        if ok:
            ok =requete.exec("CREATE TABLE exercices( "
                     "id integer primary key, "
                     "serieId int, "
                     "numero int, "
                     "question varchar(2000),"
                     "reponse varchar(2000), "
                     "FOREIGN KEY(serieId) REFERENCES series(id) "
                     "ON DELETE CASCADE "
                     "ON UPDATE CASCADE"
                     ")")
            if not ok:
                msg = 'table regles non créée'
         
         
        #Insertion de données de tests
        
        #pour la table regles
        if ok:
            ok  = requete.exec("INSERT INTO regles (id, titre, description) "
                     "VALUES (1, 'regle 1', 'La description de la règle 1')")
            if ok:
                ok = requete.exec("INSERT INTO regles (id, titre, description) "
                         "VALUES (2, 'regle 2', 'La description de la règle 2')")
            if ok:
                ok = requete.exec("INSERT INTO regles (id, titre, description) "
                         "VALUES (3, 'regle 3', 'La description de la règle 3')")
            if not ok:
                msg = 'problème d\'insertion dans la table regles'

        #pour la table seriesExercices
        if ok:
            ok = requete.exec("INSERT INTO seriesExercices (id, regleId, titre, description) "
                          "VALUES (1, 1, \"La première série d'exercices de la règle 1\", \"Description de la série 1\")")
            if ok:
                ok = requete.exec("INSERT INTO seriesExercices (id, regleId, titre, description) "
                                  "VALUES (2, 1, \"La deuxième série d'exercices de la règle 1\", \"Description de la série 2\")")
            if ok:
                ok = requete.exec("INSERT INTO seriesExercices (id, regleId, titre, description) "
                                  "VALUES (3, 2, \"La première série d\'exercices de la règle 2\", \"Description de la série 3\")") 
            if not ok:
                msg = 'problème d\'insertion dans la table seriesExercices'
        
        #pour la table exercices
        if ok:
            ok = requete.exec("INSERT INTO exercices (id, serieId, numero, question, reponse) "
                              "VALUES (1, 1, 1, 'question 1 de la série 1 de la regle 1', 'la réponse')")
            if  ok:
                ok = requete.exec("INSERT INTO exercices (id, serieId, numero, question, reponse) "
                              "VALUES (2, 1, 2, 'question 2 de la série 1 de la regle 1', 'la réponse')")
            if  ok:
                ok = requete.exec("INSERT INTO exercices (id, serieId, numero, question, reponse) "
                              "VALUES (3, 1, 3, 'question 3 de la série 1 de la regle 1', 'la réponse')")
            if  ok:
                ok = requete.exec("INSERT INTO exercices (id, serieId, numero, question, reponse) "
                              "VALUES (4, 2, 1, 'question 1 de la série 2 de la regle 1', 'la réponse')")
            if not ok:
                msg = 'problème d\'insertion dans la table exercices'
        
        return {'ok': ok, 'msg': msg}
    
   