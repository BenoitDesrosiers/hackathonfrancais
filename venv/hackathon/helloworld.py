#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
ZetCode PyQt5 tutorial 

In this example, we select a font name
and change the font of a label. 

Author: Jan Bodnar
Website: zetcode.com 
Last edited: August 2017
"""
import sys
import logging
from PyQt5.QtWidgets import QApplication, QWidget

from interfaceBase.menu import menuPrincipale
from database.sqlite import bd


monApp=QApplication(sys.argv)
gestionnaireBD = bd('QSQLITE','bdexample')
bdOuverte = gestionnaireBD.ouverture()
if bdOuverte['ok']:
    menu = menuPrincipale(gestionnaireBD)
    menu.affiche()    
    sys.exit(monApp.exec_())
else:
    logging.critical('la bd ne s\'est pas ouverte. Fin de l\'execution')