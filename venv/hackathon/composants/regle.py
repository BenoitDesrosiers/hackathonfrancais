import logging
from PyQt5.QtWidgets import *
from PyQt5 import QtSql
from PyQt5.QtSql import QSqlQueryModel

class regle():
    def __init__(self, interface, bd):
        self.interface = interface
        self.bd = bd
        
        self.id = None
        self.titre = None
        self.description = None
        
        #pour edition
        self.idEdit = None
        self.titreEdit = None
        self.descEdit = None
        
        #pour la liste des regles
        self.listwidget = None
        self.listeId = []
        self.listeWidget = {}
      
    # Initialise le layout pour afficher un item. 
    # l'affichage de l'item se fait dans interfaceAfficheItem.
    def interfaceAfficheItemInit(self):
        inputlayout = QGridLayout()
        inputlayout.setSpacing(10)
        idLabel = QLabel('Id')
        titreLabel = QLabel('Titre')
        descriptionLabel = QLabel('Description')
        self.listeWidget['idValeur'] = QLabel('')
        self.listeWidget['titreValeur'] = QLabel('')
        self.listeWidget['descriptionValeur'] = QLabel('')

        inputlayout.addWidget(idLabel, 1, 0)
        inputlayout.addWidget(self.listeWidget['idValeur'], 1, 1)
        inputlayout.addWidget(titreLabel, 2, 0)
        inputlayout.addWidget(self.listeWidget['titreValeur'], 2, 1)
        inputlayout.addWidget(descriptionLabel, 3, 0)
        inputlayout.addWidget(self.listeWidget['descriptionValeur'], 3, 1)

        return inputlayout
    
    # Affiche l'item ayant l'id
    # la fonction interfaceAfficheItemInit doit être appelée avant afin d'initialiser le layout. 
    def interfaceAfficheItem(self, id):
        requete = QtSql.QSqlQuery()
        requete.prepare("SELECT * FROM regles WHERE id = :id")
        requete.bindValue(":id", id)
        requete.exec()  # faudrait vérifier qu'il y a qqchose de retourné
        requete.next()

        self.listeWidget['idValeur'].setText(str(requete.value(0)))
        self.listeWidget['titreValeur'].setText(requete.value(1))
        self.listeWidget['descriptionValeur'].setText(requete.value(2))

    def interfaceModifier(self, id):
        layout = self.interfaceAjout()
        requete = QtSql.QSqlQuery()
        requete.prepare("SELECT * FROM regles WHERE id = :id")
        requete.bindValue(":id", id)
        requete.exec()
        requete.next()
        self.id = requete.value(0)
        self.titreEdit.setText(requete.value(1))
        self.descEdit.setText(requete.value(2))
        return layout
    
    def interfaceAjout(self):
        inputlayout = QGridLayout()
        inputlayout.setSpacing(10)

        titre = QLabel('Titre')
        description = QLabel('Description')
        self.titreEdit = QLineEdit()
        self.descEdit = QLineEdit()

        inputlayout.addWidget(titre, 1, 0)
        inputlayout.addWidget(self.titreEdit, 1, 1)
        inputlayout.addWidget(description, 2, 0)
        inputlayout.addWidget(self.descEdit, 2, 1)
        
        return inputlayout
   
   
    def interfaceLister(self, callBack):
        inputlayout = QHBoxLayout()
        self.listwidget = QListWidget()
        self.listwidget.currentItemChanged.connect(callBack)
        # Charge la liste des règles
        requete = QtSql.QSqlQuery()
        requete.prepare("SELECT * FROM regles")
        requete.exec()
        self.listeId=[]
        while (requete.next()):
            self.listeId.append(requete.value(0)) #l'id
            self.listwidget.addItem(requete.value(1)+ " " + requete.value(2) ) # titre + description
        inputlayout.addWidget(self.listwidget)
        return inputlayout
     

    # Verifie si toute l'info est dans la fenetre de création et sous le bon format
    def estValide(self):
        ok = True
        msg = []
        if self.titreEdit.text() == '':
            ok = False
            msg.append('le titre ne peut être vide')
        if self.descEdit.text() == '':
            ok = False
            msg.append('la description ne peut être vide')
        
        return {'ok': ok, 'msg':msg}
    
    # creation dans la BD 
    def creer(self):
        requete = QtSql.QSqlQuery()
        requete.prepare("INSERT INTO regles (titre, description)"
                        "VALUES (:titre, :description )")
        requete.bindValue(":titre", self.titreEdit.text())
        requete.bindValue(":description", self.descEdit.text())
        
        #il faudrait ajouter de la validation ici. 
        ok = requete.exec()
        if not ok:
            toto = requete.lastError()
            logging.critical(toto.text())
        return True
    
    def sauvegarder(self):  
        requete = QtSql.QSqlQuery()
        requete.prepare("UPDATE  regles "
                        "SET titre = :titre, "
                        "description = :description "
                        "WHERE id = :id")
        requete.bindValue(":id", self.id)
        requete.bindValue(":titre", self.titreEdit.text())
        requete.bindValue(":description", self.descEdit.text())

        # il faudrait ajouter de la validation ici. 
        ok = requete.exec()
        if not ok:
            toto = requete.lastError()
            logging.critical(toto.text())
        return True
        
        
    # Efface l'item avec l'id passé en parametre
    def effacer(self, id):
        requete = QtSql.QSqlQuery()
        requete.prepare("DELETE FROM regles WHERE id = :id")
        requete.bindValue(":id", id)
        requete.exec()

    # Retourne l'item qui est présentement sélectionné dans la liste d'effacement
    def idItemChoisi(self):    
        return self.listeId[self.listwidget.currentRow()]
