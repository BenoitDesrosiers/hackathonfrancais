import logging
from PyQt5.QtWidgets import *
from PyQt5 import QtSql
from PyQt5.QtSql import QSqlQueryModel
from PyQt5.QtCore import QVariant

class serie():
    def __init__(self, interface, bd):
        self.interface = interface
        self.bd = bd
        
        self.id = None
        self.idRegle = None
        self.titre = None
        self.description = None
        
        #pour edition
        self.idEdit = None
        self.idRegleEdit = None
        self.titreEdit = None
        self.descEdit = None
        self.listeReglesCombo = None
        
        #pour la liste des séries
        self.listwidget = None
        self.listeId = []
        self.listeWidget = {}
      
    # Initialise le layout pour afficher un item. 
    # l'affichage de l'item se fait dans interfaceAfficheItem.
    def interfaceAfficheItemInit(self):
        inputlayout = QGridLayout()
        inputlayout.setSpacing(10)
        idLabel = QLabel('Id')
        titreLabel = QLabel('Titre')
        descriptionLabel = QLabel('Description')
        regleAssocieeLabel = QLabel('pour la règle')
        self.listeWidget['idValeur'] = QLabel('')
        self.listeWidget['titreValeur'] = QLabel('')
        self.listeWidget['descriptionValeur'] = QLabel('')
        self.listeWidget['regleAssocieeValeur'] = QLabel('')

        inputlayout.addWidget(idLabel, 1, 0)
        inputlayout.addWidget(self.listeWidget['idValeur'], 1, 1)
        inputlayout.addWidget(titreLabel, 2, 0)
        inputlayout.addWidget(self.listeWidget['titreValeur'], 2, 1)
        inputlayout.addWidget(descriptionLabel, 3, 0)
        inputlayout.addWidget(self.listeWidget['descriptionValeur'], 3, 1)
        inputlayout.addWidget(regleAssocieeLabel, 4, 0)
        inputlayout.addWidget(self.listeWidget['regleAssocieeValeur'], 4, 1)
        return inputlayout
    
    # Affiche l'item ayant l'id
    # la fonction interfaceAfficheItemInit doit être appelée avant afin d'initialiser le layout. 
    def interfaceAfficheItem(self, id):
        requete = QtSql.QSqlQuery()
        requete.prepare("SELECT * FROM seriesExercices WHERE id = :id")
        requete.bindValue(":id", id)
        requete.exec()  # faudrait vérifier qu'il y a qqchose de retourné
        requete.next()

        self.listeWidget['idValeur'].setText(str(requete.value(0)))
        # value(1) est l'id de la règle associée. On veut pas l'id, on veux le titre
        self.listeWidget['titreValeur'].setText(requete.value(2))
        self.listeWidget['descriptionValeur'].setText(requete.value(3))

        # va chercher le titre de la règle associée
        requete2 = QtSql.QSqlQuery()
        requete2.prepare("SELECT * FROM regles WHERE id = :idRegle")
        requete2.bindValue(":idRegle", requete.value(1))  #l'id de la regle associée
        requete2.exec()
        requete2.next()
        self.listeWidget['regleAssocieeValeur'].setText(requete2.value(1)) #le titre de la règle
        
        
    def interfaceModifier(self, id):
        layout = self.interfaceAjout()
        
        requete = QtSql.QSqlQuery()
        requete.prepare("SELECT * FROM seriesExercices WHERE id = :id")
        requete.bindValue(":id", id)
        requete.exec()
        requete.next()
        self.id = requete.value(0)
        self.titreEdit.setText(requete.value(2))
        self.descEdit.setText(requete.value(3))
        
        # change la règle sélectionnée dans le combobox pour celle qui est associée à cette série
        self.listeReglesCombo.setCurrentIndex(self.listeReglesCombo.findData(requete.value(1)))
        return layout
    
    def interfaceAjout(self):
        inputlayout = QGridLayout()
        inputlayout.setSpacing(10)

        titre = QLabel('Titre')
        description = QLabel('Description')
        self.titreEdit = QLineEdit()
        self.descEdit = QLineEdit()
        
        # va cherche la liste des regles pour le mettre dans un qcombobox.
        self.listeReglesCombo = QComboBox()
        requete = QtSql.QSqlQuery()
        requete.prepare("SELECT id, titre FROM regles")
        requete.exec()
        while (requete.next()):
            self.listeReglesCombo.addItem(requete.value(1), QVariant(requete.value(0)))  # le QVariant retient l'id de la règle associée
        inputlayout.addWidget(titre, 1, 0)
        inputlayout.addWidget(self.titreEdit, 1, 1)
        inputlayout.addWidget(description, 2, 0)
        inputlayout.addWidget(self.descEdit, 2, 1)
        inputlayout.addWidget(QLabel('Règle associée'),3,0)
        inputlayout.addWidget(self.listeReglesCombo,3,1)
        
        return inputlayout
   
   
    def interfaceLister(self, callBack):
        inputlayout = QHBoxLayout()
        self.listwidget = QListWidget()
        self.listwidget.currentItemChanged.connect(callBack)
        # Charge la liste des séries
        requete = QtSql.QSqlQuery()
        requete.prepare("SELECT * FROM seriesExercices")
        requete.exec()
        self.listeId=[]
        while (requete.next()):
            self.listeId.append(requete.value(0)) #l'id
            self.listwidget.addItem(requete.value(2)+ " " + requete.value(3) ) # titre + description
        inputlayout.addWidget(self.listwidget)
        return inputlayout
     

    # Verifie si toute l'info est dans la fenetre de création et sous le bon format
    def estValide(self):
        ok = True
        msg = []
        if self.titreEdit.text() == '':
            ok = False
            msg.append('le titre ne peut être vide')
        if self.descEdit.text() == '':
            ok = False
            msg.append('la description ne peut être vide')
        #pas besoin de vérifier l'indice de la règle étant donné qui provient d'un combobox
        return {'ok': ok, 'msg':msg}
    
    # creation dans la BD 
    def creer(self):
        requete = QtSql.QSqlQuery()
        requete.prepare("INSERT INTO seriesExercices (regleId, titre, description)"
                        "VALUES (:regleId, :titre, :description )")
        requete.bindValue(":regleId", self.listeReglesCombo.currentData())
        requete.bindValue(":titre", self.titreEdit.text())
        requete.bindValue(":description", self.descEdit.text())
        
        #il faudrait ajouter de la validation ici. 
        ok = requete.exec()
        if not ok:
            toto = requete.lastError()
            logging.critical(toto.text())
        return True
    
    def sauvegarder(self):  
        requete = QtSql.QSqlQuery()
        requete.prepare("UPDATE  seriesExercices "
                        "SET regleId = :regleId,"
                        "titre = :titre, "
                        "description = :description "
                        "WHERE id = :id")
        requete.bindValue(":id", self.id)
        requete.bindValue(":regleId", self.listeReglesCombo.currentData())
        requete.bindValue(":titre", self.titreEdit.text())
        requete.bindValue(":description", self.descEdit.text())

        # il faudrait ajouter de la validation ici. 
        ok = requete.exec()
        if not ok:
            toto = requete.lastError()
            logging.critical(toto.text())
        return True
        
        
    # Efface l'item avec l'id passé en parametre
    def effacer(self, id):
        requete = QtSql.QSqlQuery()
        requete.prepare("DELETE FROM seriesExercices WHERE id = :id")
        requete.bindValue(":id", id)
        requete.exec()

    # Retourne l'item qui est présentement sélectionné dans la liste d'effacement
    def idItemChoisi(self):    
        return self.listeId[self.listwidget.currentRow()]
